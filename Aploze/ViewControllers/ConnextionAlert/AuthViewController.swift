//
//  AuthViewController.swift
//  Aploze
//
//  Created by John Kricorian on 29/01/2022.
//

import UIKit

class AuthViewController: BaseViewController {

    @IBOutlet weak private var mainViewBackground: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // hide navigationBar
        self.navigationController?.navigationBar.isHidden = true
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    
    @IBAction func updateAuth(_ sender: Any) {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, options: [:]) { _ in
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
}
