//
//  LoginViewController.swift
//  Aploze
//
//  Created by John Kricorian on 28/01/2022.
//

import UIKit
import FirebaseCore
import FirebaseAuth

class LoginViewController: BaseViewController {
    
    @IBOutlet weak private var emailLabel: UILabel!
    @IBOutlet weak private var emailBackgroundView: UIView!
    @IBOutlet weak private var emailTextField: UITextField!
    @IBOutlet weak private var emailRequiredLabel: UILabel!
    
    @IBOutlet weak private var passwordLabel: UILabel!
    @IBOutlet weak private var passwordBackgroundView: UIView!
    @IBOutlet weak private var passwordTextField: UITextField!
    @IBOutlet weak private var passwordRequiredLabel: UILabel!
    
    @IBOutlet weak private var connexionButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // hide navigationBar
        self.navigationController?.navigationBar.isHidden = true
        
        self.roundViewGreyBorder(viewName: emailBackgroundView)
        self.roundViewGreyBorder(viewName: passwordBackgroundView)
        
        emailRequiredLabel.isHidden = true
        passwordRequiredLabel.isHidden = true
        
        connexionButton.clipsToBounds = true
        connexionButton.layer.cornerRadius = 10
        
        connexionButton.isEnabled = false
        
        let userDefaults = UserDefaults.standard
        if !userDefaults.bool(forKey: "hasRunBefore") {
            print("The app is launching for the first time. Setting UserDefaults...")

            do {
                try Auth.auth().signOut()
            } catch {

            }

            // Update the flag indicator
            userDefaults.set(true, forKey: "hasRunBefore")
            userDefaults.synchronize() // This forces the app to update userDefaults
        } else {
            print("The app has been launched before. Loading UserDefaults...")
            // Run code here for every other launch but the first
            
            if Auth.auth().currentUser != nil {
                let viewController = ChannelViewController(nibName: "ChannelViewController", bundle: nil)
                let nav = NavigationViewController(rootViewController: viewController)
                
                UIApplication.shared.windows.first?.rootViewController = nav
                UIApplication.shared.windows.first?.makeKeyAndVisible()
                
                self.navigationController?.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        connexionButton.isEnabled = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    
    @IBAction func connexionButtonAction(_ sender: Any) {
        if emailTextField.text!.count < 1 || passwordTextField.text!.count < 1
        {
            emailRequiredLabel.isHidden = false
            passwordRequiredLabel.isHidden = false
            
            emailLabel.textColor = UIColor.clrRed
            passwordLabel.textColor = UIColor.clrRed
            
            emailRequiredLabel.text = "Your email address required"
            passwordRequiredLabel.text = "Your password is required"
            
            self.roundViewRedBorder(viewName: emailBackgroundView)
            self.roundViewRedBorder(viewName: passwordBackgroundView)
        } else {
            firebaseLogin()
        }
    }
    
    @IBAction func editingChanged(_ sender: Any)
    {
        self.emailRequiredLabel.isHidden = true
        self.passwordRequiredLabel.isHidden = true
        
        self.emailLabel.textColor = UIColor.clrWhite
        self.passwordLabel.textColor = UIColor.clrWhite
        
        self.emailTextField.textColor = UIColor.clrWhite
        self.passwordTextField.textColor = UIColor.clrWhite
        self.emailTextField.tintColor = UIColor.clrWhite
        self.passwordTextField.tintColor = UIColor.clrWhite
        
        self.emailRequiredLabel.text = nil
        self.passwordRequiredLabel.text = nil
        
        self.roundViewGreyBorder(viewName: self.emailBackgroundView)
        self.roundViewGreyBorder(viewName: self.passwordBackgroundView)
        
        if !emailTextField.text!.isEmpty && !passwordTextField.text!.isEmpty
        {
            connexionButton.isEnabled = true
        } else {
            connexionButton.isEnabled = false
        }
        
    }
    
    // firebaseLogin
    func firebaseLogin()
    {
        showLoading()
        // Firebase Login
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (authResult, _) in
            self.hideLoading()
            
            if authResult?.user != nil
            {
                let viewController = ChannelViewController(nibName: "ChannelViewController", bundle: nil)
                let nav = NavigationViewController(rootViewController: viewController)
                
                UIApplication.shared.windows.first?.rootViewController = nav
                UIApplication.shared.windows.first?.makeKeyAndVisible()
                
                self.navigationController?.dismiss(animated: false, completion: nil)
            } else {
                self.emailRequiredLabel.isHidden = false
                self.passwordRequiredLabel.isHidden = false
                
                self.emailLabel.textColor = UIColor.clrRed
                self.passwordLabel.textColor = UIColor.clrRed
                
                self.emailTextField.textColor = UIColor.clrRed
                self.passwordTextField.textColor = UIColor.clrRed
                self.emailTextField.tintColor = UIColor.clrRed
                self.passwordTextField.tintColor = UIColor.clrRed
                
                self.emailRequiredLabel.text = "Wrong email, please try again"
                self.passwordRequiredLabel.text = "Wrong password, please try again"
                
                self.roundViewRedBorder(viewName: self.emailBackgroundView)
                self.roundViewRedBorder(viewName: self.passwordBackgroundView)
            }
        }
    }
}
