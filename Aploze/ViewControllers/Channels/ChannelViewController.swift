//
//  ChannelViewController.swift
//  Aploze
//
//  Created by John Kricorian on 28/01/2022.
//

import UIKit
import Alamofire
import FirebaseAuth
import AlamofireImage
import AVFoundation

class ChannelViewController: BaseViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var logoutButton: UIButton!
    @IBOutlet weak private var countLabel: UILabel!
    
    var channelArr = [ChannelData](){
        didSet {
            countLabel.text = "(\(channelArr.count))"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "CellChannel", bundle: nil), forCellReuseIdentifier: "cellChannelid")
        
        logoutButton.clipsToBounds = true
        logoutButton.layer.cornerRadius = 12
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // hide navigationBar
        self.navigationController?.navigationBar.isHidden = true
        AppUtility.lockOrientation(.portrait)
        
        //get token first
        showLoading()
        Auth.auth().currentUser?.getIDToken(completion: { token, err in
            if err != nil
            {
                self.hideLoading()
            } else {
                self.channelAPI(token: token!)
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    
    @IBAction func logoutButtonAction(_ sender: Any) {
        showLoading()
        
        do
        {
            try Auth.auth().signOut()
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyBoard.instantiateInitialViewController()
                UIApplication.shared.windows.first?.rootViewController = viewController
                UIApplication.shared.windows.first?.makeKeyAndVisible()
                
                self.hideLoading()
                
                self.navigationController?.dismiss(animated: false, completion: nil)
            }
        } catch
        {
            hideLoading()
        }
        
    }
}

// tableView extension
extension ChannelViewController: UITableViewDelegate, UITableViewDataSource
{
    // tableView code
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return channelArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellChannelid") as? CellChannel
        
        self.roundViewGreyBorder(viewName: (cell?.mainBackgroundView)!)
        
        cell?.imageViewBackground.clipsToBounds = true
        cell?.imageViewBackground.layer.cornerRadius = 8
        
        cell?.name.text = channelArr[indexPath.row].name!
        
        // get Image from SDWebImage
        if let url = channelArr[indexPath.row].picture as? URLRequestConvertible
        {
            AF.request(url).responseImage { response in
                debugPrint(response)
                if case .success(let image) = response.result {
                    cell?.icon.image = UIImage(named: "\(image)")
                } else {
                    cell?.icon.image = UIImage(named: "channel1")
                }
            }
        } else {
            cell?.icon.image = UIImage(named: "channel1")
        }
        
        guard let cell = cell else {
            fatalError("CellChannel cannot be nil")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if AVCaptureDevice.authorizationStatus(for: .video) == .authorized && AVCaptureDevice.authorizationStatus(for: .audio) == .authorized
        {
            let viewController = LiveCameraViewController(nibName: "LiveCameraViewController", bundle: nil)
            viewController.channel = self.channelArr[indexPath.row]
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            let viewController = PermissionsViewController()
            viewController.channel = self.channelArr[indexPath.row]
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension ChannelViewController
{
    func channelAPI(token: String)
    {
        let headers: HTTPHeaders = [
            "Authorization": token
        ]
        
        AF.request("https://dev.api.aploze.com/api/channels", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseDecodable(of: RootChannel.self) { response in
            switch response.result
            {
            case .success(let data):
                self.channelArr = data.data
                self.tableView.reloadData()
                self.hideLoading()
            case .failure(let error):
                self.hideLoading()
                print("Error:", error)
            }
        }
    }
}
