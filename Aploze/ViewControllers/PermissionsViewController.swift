//
//  PermissionsViewController.swift
//  Aploze
//
//  Created by John Kricorian on 24/02/2022.
//

import UIKit
import AVFoundation

class PermissionsViewController: UIViewController {
    
    var channel : ChannelData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .clrBlack
        
        checkCameraPermission()
    }
    
    private func checkCameraPermission()
    {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            checkMicPermission()
        case .notDetermined:
            requestCameraPermission()
        case .denied, .restricted:
            self.permissionsDenied()
        @unknown default:
            self.permissionsDenied()
        }
    }
    
    private func checkMicPermission()
    {
        switch AVCaptureDevice.authorizationStatus(for: .audio) {
        case .authorized:
            permissionsGranted()
        case .notDetermined:
            requestMicPermission()
        case .denied, .restricted:
            self.permissionsDenied()
        @unknown default:
            self.permissionsDenied()
        }
    }
    
    private func requestCameraPermission()
    {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
            DispatchQueue.main.async {
                if granted
                {
                    self.permissionsGranted()
                } else {
                    self.permissionsDenied()
                }
            }
        })
    }
    
    private func requestMicPermission()
    {
        AVCaptureDevice.requestAccess(for: .audio, completionHandler: { granted in
            if granted
            {
                self.permissionsGranted()
            } else {
                self.permissionsDenied()
            }
        })
    }
    
    private func permissionsGranted()
    {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: false)
            
            let viewController = LiveCameraViewController(nibName: "LiveCameraViewController", bundle: nil)
            viewController.channel = self.channel
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
    private func permissionsDenied()
    {
        DispatchQueue.main.async {
            let viewController = AuthViewController(nibName: "AuthViewController", bundle: nil)
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
}
