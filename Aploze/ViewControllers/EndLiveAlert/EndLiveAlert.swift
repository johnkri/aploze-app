//
//  EndLiveAlert.swift
//  Aploze
//
//  Created by John Kricorian on 29/01/2022.
//

import UIKit

class EndLiveAlert: BaseViewController {
    
    @IBOutlet weak private var mainViewBackground: UIView!
    @IBOutlet weak private var confirmButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //viewBg.clipsToBounds = true
        //viewBg.layer.cornerRadius = 20
        
        confirmButton.clipsToBounds = true
        confirmButton.layer.cornerRadius = 10
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // hide navigationBar
        self.navigationController?.navigationBar.isHidden = true
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    
    @IBAction func confirmBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
}
