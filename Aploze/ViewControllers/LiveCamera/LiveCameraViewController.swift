//
//  LiveCameraViewController.swift
//  Aploze
//
//  Created by John Kricorian on 29/01/2022.
//

import UIKit
import CoreLocation
import AmazonIVSBroadcast
//
//enum SessionState {
//    case none
//    case connecting
//    case connected
//    case disconnecting
//    case disconnected
//    case error
//}

class LiveCameraViewController: BaseViewController {
    
    // custom camera related code outlets
    @IBOutlet weak private var privewView: IVSImagePreviewView!
    
    @IBOutlet weak private var startButton: UIButton!
    @IBOutlet weak private var startProgress: UIActivityIndicatorView!
    @IBOutlet weak private var backButton: UIButton!
    @IBOutlet weak private var flipCameraButton: UIButton!
    
    @IBOutlet weak private var wifiButton: UIButton!
    @IBOutlet weak private var wifiIcon: UIImageView!
    @IBOutlet weak private var wifiProgress: UIActivityIndicatorView!
    
    private var isCamerFlip = true
    
    // for internet downloading speed
    @IBOutlet weak private var downloadingSpeed: UILabel!
    
    @IBOutlet weak var timerBackgroundView: UIView!
    @IBOutlet weak var timerLabel: UILabel!
    
    // IVSBroadcastSession
    private var broadcastSession : IVSBroadcastSession?
    private var state : IVSBroadcastSession.State = .invalid
    
    var channel : ChannelData! {
        didSet {
            ivsRtmpUrl = URL(string: channel.ingestUrl)!
        }
    }
    private var ivsRtmpUrl : URL!
    
    // currentCamera code
    var wasRunningBeforeInterruption = false
    var isRunning = false
    var currentCamera: IVSDevice?
    
    // timer code
    var counter = 0
    var streamTimer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // hide navigationBar
        self.navigationController?.navigationBar.isHidden = true
        
//        wifiSpeedBackgroundView.clipsToBounds = true
//        wifiSpeedBackgroundView.layer.cornerRadius = 17.5
        
        timerBackgroundView.clipsToBounds = true
        timerBackgroundView.layer.cornerRadius = 17.5
        
        startButton.clipsToBounds = true
        startButton.layer.cornerRadius = 12
        
        self.roundBtn(view: backButton)
        self.roundBtn(view: flipCameraButton.superview!)
        self.roundBtn(view: wifiButton.superview!)
        
        self.backButton.isHidden = false
        self.timerBackgroundView.isHidden = true
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(audioSessionInterrupted(_:)),
            name: AVAudioSession.interruptionNotification,
            object: nil)
        
        setupSession()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
        broadcastSession?.stop()
        
        streamTimer?.invalidate()
    }
    
    private func setupSession()
    {
        do {
            broadcastSession = try IVSBroadcastSession(
                configuration: IVSPresets.configurations().standardPortrait(),
                descriptors: IVSPresets.devices().frontCamera(),
                delegate: self)
            
            broadcastSession?.awaitDeviceChanges({
                if let devic = self.broadcastSession?.listAttachedDevices()
                    .compactMap({ $0 as? IVSImageDevice })
                    .first
                {
                    if let preview = try? devic.previewView(with: .fill)
                    {
                        self.currentCamera = devic
                        self.privewView.addSubview(preview)
                    }
                }
            })
        } catch {
            self.showAlert("Error", error.localizedDescription, actions: nil)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func flipCamera(_ sender: Any) {
        swipeLiveCamera()
    }
    
    @IBAction func checkConnection(_ sender: Any) {
        wifiIcon.image = nil
        wifiButton.isEnabled = false
        wifiProgress.startAnimating()
        broadcastSession?.recommendedVideoSettings(with: self.ivsRtmpUrl, streamKey: self.channel.streamKey, results: { result in
            if result.status == .success
            {
                if result.recommendations.isEmpty
                {
                    DispatchQueue.main.async {
                        self.showAlert("Insufficient connection", "You need a minimum speed of 2.50 Mbps to stream without risk", actions: nil, completion: nil)
                    }
                } else {
                    let recom = result.recommendations.first!
                    let sped = Double(recom.initialBitrate) / Double(100000.0)
                    let speed = String(format: "%.2f",sped)
                    DispatchQueue.main.async {
                        self.showAlert("You are ready to go live", "Speed: \(speed) Mbps \nQuality: \(Int(recom.size.height))p", actions: nil, completion: nil)
                    }
                }
            } else if result.status == .error {
                DispatchQueue.main.async {
                    self.wifiButton.isEnabled = true
                    self.wifiProgress.stopAnimating()
                    self.wifiIcon.image = UIImage(named: "wifi")
                    self.showAlert("Error", "Faild to test connection", actions: nil, completion: nil)
                }
            } else if result.status == .testing
            {
                if result.recommendations.isEmpty
                {
                    print("No Recommendations")
                } else {
                    print("------->")
                    result.recommendations.forEach({ recom in
                        let speed = Double(recom.initialBitrate) / Double(100000.0)
                        print("You are ready to go live", "Speed: \(speed) Mbps \nQuality: \(recom.size)")
                    })
                }
            }
            
            if result.progress == 1.0
            {
                DispatchQueue.main.async {
                    self.wifiButton.isEnabled = true
                    self.wifiProgress.stopAnimating()
                    self.wifiIcon.image = UIImage(named: "wifi")
                }
            }
        })
    }
    
    @IBAction func startCameraTapGesture(_ sender: Any) {
        guard let broadcastSession = broadcastSession else {
            setupSession()
            return
        }
        
        do {
            if state == .connected {
                stopLive()
            } else {
                try broadcastSession.start(with: ivsRtmpUrl, streamKey: channel.streamKey)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @objc func timerAction() {
        counter += 1
        if counter < 10 {
            timerLabel.text = "00:0\(counter)"
        } else {
            if counter < 60 {
                timerLabel.text = "00:\(counter)"
            } else {
                let minutes = counter / 60
                let seconds = counter % 60
                let min = minutes < 10 ? "0\(minutes)" : "\(minutes)"
                if seconds < 10
                {
                    timerLabel.text = "\(min):0\(seconds)"
                } else {
                    timerLabel.text = "\(min):\(seconds)"
                }
            }
        }
    }
    
    func roundBtn(view: UIView){
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
    }
    
    private func streamConnected()
    {
        self.backButton.isHidden = true
        self.timerBackgroundView.isHidden = false
        
        startTimer()
        
        self.startButton.setTitle("END LIVE", for: UIControl.State.normal)
        self.startButton.backgroundColor = .clrDarkGrey
        self.startButton.imageView?.image = UIImage(systemName: "record.circle")
        self.startButton.tintColor = UIColor.clrRed
    }
    
    private func streamDiconnected()
    {
        self.backButton.isHidden = false
        self.timerBackgroundView.isHidden = true
        
        stopTimer()
        
        self.startButton.setTitle("START", for: UIControl.State.normal)
        self.startButton.imageView?.image = UIImage(systemName: "record.circle")
        self.startButton.backgroundColor = .clrRed
        self.startButton.tintColor = UIColor.white
    }
    
    private func startTimer()
    {
        self.streamTimer?.invalidate()
        counter = 0
        
        self.streamTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
    }
    
    private func stopTimer()
    {
        self.streamTimer?.invalidate()
    }
    
    private func stopLive()
    {
        let sheet = UIAlertController(title: "Do you want to stop the live?", message: nil, preferredStyle: .actionSheet)
        
        sheet.addAction(UIAlertAction(title: "End the live", style: .default, handler: { _ in
            self.broadcastSession?.stop()
        }))
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(sheet, animated: true, completion: nil)
    }
}

extension LiveCameraViewController : IVSBroadcastSession.Delegate
{
    func broadcastSession(_ session: IVSBroadcastSession,
                          didChange state: IVSBroadcastSession.State) {
        self.state = state
        
        if state == .connecting
        {
            wifiButton.superview?.isHidden = true
            startProgress.startAnimating()
            self.startButton.tintColor = .clear
        } else if state == .connected
        {
            wifiButton.superview?.isHidden = true
            startProgress.stopAnimating()
            self.startButton.tintColor = .clrRed
            streamConnected()
        } else if state == .disconnected
        {
            wifiButton.superview?.isHidden = false
            startProgress.stopAnimating()
            self.startButton.tintColor = .clrRed
            streamDiconnected()
        } else if state == .error
        {
            wifiButton.superview?.isHidden = false
            startProgress.stopAnimating()
            self.startButton.tintColor = .clrRed
            streamDiconnected()
        }
    }
    
    func broadcastSession(_ session: IVSBroadcastSession,
                          didEmitError error: Error) {
        print("IVSBroadcastSession did emit error \(error)")
    }
}

extension LiveCameraViewController
{
    // This assumes you have a variable `isRunning` which tracks if the broadcast is currently live, and another variable `wasRunningBeforeInterruption` which tracks whether the broadcast was active before this interruption to determine if it should resume after the interruption has ended.
    @objc private func audioSessionInterrupted(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
              let typeValue = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
              let type = AVAudioSession.InterruptionType(rawValue: typeValue)
        else {
            return
        }
        switch type {
        case .began:
            wasRunningBeforeInterruption = isRunning
            if isRunning {
                broadcastSession!.stop()
            }
        case .ended:
            defer {
                wasRunningBeforeInterruption = false
            }
            guard let optionsValue = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt else { return }
            let options = AVAudioSession.InterruptionOptions(rawValue: optionsValue)
            if options.contains(.shouldResume) && wasRunningBeforeInterruption {
                do {
                    try broadcastSession!.start(
                        with: ivsRtmpUrl,
                        streamKey: channel.streamKey)
                } catch {
                    print(error.localizedDescription)
                }
            }
        @unknown default: break
        }
    }
    
    func swipeLiveCamera()
    {
        self.flipCameraButton.isEnabled = false
        // This assumes you’ve kept a reference called `currentCamera` that points to the current camera.
        let wants: IVSDevicePosition = (self.currentCamera?.descriptor().position == .front) ? .back : .front
        // Remove the current preview view since the device will be changing.
        privewView.subviews.forEach { $0.removeFromSuperview() }
        let foundCamera = IVSBroadcastSession
            .listAvailableDevices()
            .first { $0.type == .camera && $0.position == wants }
        guard let newCamera = foundCamera else { return }
        
        guard let currentCamera = self.currentCamera else { return }
        
        self.broadcastSession!.exchangeOldDevice(currentCamera, withNewDevice: newCamera) { newDevice, _ in
            self.currentCamera = newDevice
            if let camera = newDevice as? IVSImageDevice {
                do {
                    self.privewView.addSubview(try camera.previewView(with: .fill))
                } catch {
                    print("Error creating preview view \(error)")
                }
            }
            self.flipCameraButton.isEnabled = true
        }
    }
}
