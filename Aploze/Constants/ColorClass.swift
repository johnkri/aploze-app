//
//  ColorClass.swift
//  Aploze
//
//  Created by John Kricorian on 28/01/2022.
//

import Foundation
import UIKit

extension UIColor {
    convenience init (hex: String) {
        var hexFormatted: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()

            if hexFormatted.hasPrefix("#") {
                hexFormatted = String(hexFormatted.dropFirst())
            }

            assert(hexFormatted.count == 6, "Invalid hex code used.")

            var rgbValue: UInt64 = 0
            Scanner(string: hexFormatted).scanHexInt64(&rgbValue)

            self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                      green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                      blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                      alpha: 1.0)
    }
}

// custom colors assign to use in the Project
extension UIColor {
    static let clrBlack = UIColor(hex: "1A1A23")
    static let clrWhite = UIColor(hex: "ffffff")
    static let clrGreyBorder = UIColor(hex: "4F4F58")
    static let clrRed = UIColor(hex: "D50032")
    static let clrDarkGrey = UIColor(hex: "2E2E36")
}
