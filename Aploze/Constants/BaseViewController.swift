//
//  BaseViewController.swift
//  Aploze
//
//  Created by John Kricorian on 28/01/2022.
//

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {
    
    // api code
    let def = UserDefaults.standard
    var loadingView : UIView!
    var del : AppDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // to store the data in AppDelgate
        del = (UIApplication.shared.delegate as? AppDelegate)
        
        //SVProgressHUD code Circle
        loadingView = UIView(frame: view.frame)
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        view.addSubview(loadingView)
        loadingView.isHidden = true
        
    }
    
    // MBProgressHUD Obj-c file func
    // showLoading
    func showLoading()
    {
        DispatchQueue.main.async {
            self.loadingView.isHidden = false
            let loading = MBProgressHUD.showAdded(to: self.view, animated: true)
            loading.mode = MBProgressHUDMode.indeterminate
            loading.label.text = "Loading ..."
        }
    }
    
    // hideLoading
    func hideLoading()
    {
        DispatchQueue.main.async {
            self.loadingView.isHidden = true
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    // showAlert
    func showAlert(_ title: String, _ message: String, actions: [UIAlertAction]?, completion: (() -> Void)? = nil)
    {
        // alert message diaoulge
        let alertController = UIAlertController(title: title, message:
                                                    message, preferredStyle: .alert)
        
        if actions == nil{
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                print("Ok")
            }))
        } else {
            actions!.forEach({ (act) in
                alertController.addAction(act)
            })
        }
        self.present(alertController, animated: true, completion: completion)
    }
    
    // roundView
    func roundViewGreyBorder(viewName: UIView){
        viewName.clipsToBounds = true
        viewName.layer.cornerRadius = 10
        viewName.layer.borderColor = UIColor.clrGreyBorder.cgColor
        viewName.layer.borderWidth = 1
    }
    
    // roundView
    func roundViewRedBorder(viewName: UIView){
        viewName.clipsToBounds = true
        viewName.layer.cornerRadius = 10
        viewName.layer.borderColor = UIColor.clrRed.cgColor
        viewName.layer.borderWidth = 1
    }
}

struct AppUtility {

    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
    
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }

    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
   
        self.lockOrientation(orientation)
    
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }

}
