//
//  RootChannel.swift
//  Aploze
//
//  Created by John Kricorian on 02/02/2022.
//

import Foundation

struct RootChannel : Codable {

    let count : Int
    let data : [ChannelData]

    enum CodingKeys: String, CodingKey {
        case count
        case data
    }
}
