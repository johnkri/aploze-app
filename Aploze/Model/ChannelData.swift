//
//  ChannelData.swift
//  Aploze
//
//  Created by John Kricorian on 02/02/2022.
//

import Foundation

struct ChannelData : Codable {

    let currency : String?
    let locale : String?
    let name : String?
    let onAir : Bool?
    let picture : String?
    let sender : String?
    let streamKey : String
    let streamPlayback : String?
    let uuid : String
    let ingestUrl : String

    enum CodingKeys: String, CodingKey {
        case currency
        case locale
        case name
        case onAir = "on_air"
        case picture
        case sender
        case streamKey = "stream_key"
        case streamPlayback = "stream_playback"
        case uuid
        case ingestUrl = "rtmps_ingest_point"
    }
}
